package com.packt.webstore.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class CategoryValidator implements ConstraintValidator<Category, String> {

    private final List<String> allowedCategories = new ArrayList<>();

    @Override
    public void initialize(Category constraintAnnotation) {
        this.allowedCategories.add("laptop");
        this.allowedCategories.add("smartfon");
        this.allowedCategories.add("tablet");
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (this.allowedCategories.contains(value)) {
            return true;
        }
        return false;
    }
}
