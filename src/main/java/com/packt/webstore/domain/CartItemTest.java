package com.packt.webstore.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CartItemTest {
    private CartItem cartItem;

    @Before
    public void setup() {
        cartItem = new CartItem();
    }

    @Test
    public void cartItemTotalPriceShouldBeEqualToProductUnitPriceInCaseOfSingleQuantity() {
        //Ustaw
        Product iphone = new Product("P1234","iPhone 5s", new BigDecimal(5000));
        cartItem.setProduct(iphone);
        //Wykonaj
        BigDecimal totalPrice = cartItem.getTotalPrice();
        //Porównaj
        Assert.assertEquals(iphone.getUnitPrice(), totalPrice);
    }
}
