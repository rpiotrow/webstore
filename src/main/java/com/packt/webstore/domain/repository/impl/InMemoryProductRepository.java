package com.packt.webstore.domain.repository.impl;

import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;

import com.packt.webstore.exception.ProductNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.math.BigDecimal;

@Repository
public class InMemoryProductRepository  implements ProductRepository {

    private List<Product> listOfProducts = new ArrayList<Product>();

    public InMemoryProductRepository() {
        Product iphone = new Product("P1234","iPhone 5s", new BigDecimal(5000));
        iphone.setDescription("Najlepszy ajfon jakiego widziała Ziemia");
        iphone.setCategory("smartfon");
        iphone.setManufacturer("Apple");
        iphone.setUnitsInStock(1000);

        Product laptopDell = new Product("P1235","Dell Inspiron", new BigDecimal(700));
        laptopDell.setDescription("Dell Inspiron, 14-calowy laptop (czarny) z procesorami Intel Core 3. generacji");
        laptopDell.setCategory("laptop");
        laptopDell.setManufacturer("Dell");
        laptopDell.setUnitsInStock(1000);

        Product tabletNexus = new Product("P1236","Nexus 7", new BigDecimal(300));
        tabletNexus.setDescription("Google Nexus 7 jest najlżejszym 7-calowym tabletem z 4-rdzeniowym procesorem Qualcomm Snapdragon™ S4 Pro");
        tabletNexus.setCategory("tablet");
        tabletNexus.setManufacturer("Google");
        tabletNexus.setUnitsInStock(1000);

        listOfProducts.add(iphone);
        listOfProducts.add(laptopDell);
        listOfProducts.add(tabletNexus);

    }

    private List<Product> getProductsByPrice(BigDecimal low, BigDecimal high) {
        List<Product> productsByPrice = new ArrayList<Product>();
        for (Product product : listOfProducts) {
            if (product.getUnitPrice().compareTo(low) > 0 && product.getUnitPrice().compareTo(high) < 0) {
                productsByPrice.add(product);
            }
        }
        return productsByPrice;
    }

    public List<Product> getAllProducts() {
        return listOfProducts;
    }

    public Product getProductById(String productId) {
        Product productById = null;

        for(Product product : listOfProducts) {
            if(product!=null && product.getProductId()!=null && product.getProductId().equals(productId)){
                productById = product;
                break;
            }
        }

        if(productById == null){
            throw new ProductNotFoundException(productId);
        }

        return productById;
    }

    public List<Product> getProductsByCategory(String category) {
        List<Product> productsByCategory = new ArrayList<Product>();
        for(Product product : listOfProducts) {
            if (category.equalsIgnoreCase(product.getCategory())) {
                productsByCategory.add(product);
            }
        }
        return productsByCategory;
    }
    public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
        Set<Product> productsByBrand = new HashSet<Product>();
        Set<Product> productsByCategory = new HashSet<Product>();

        Set<String> criterias = filterParams.keySet();

        if(criterias.contains("brand")) {
            for(String brandName: filterParams.get("brand")) {
                for(Product product: listOfProducts) {
                    if(brandName.equalsIgnoreCase(product.getManufacturer())){
                        productsByBrand.add(product);
                    }
                }
            }
        }

        if(criterias.contains("category")) {
            for(String category: filterParams.get("category")) {
                productsByCategory.addAll(this.getProductsByCategory(category));
            }
        }

        productsByCategory.retainAll(productsByBrand);
        return productsByCategory;
    }


    public Set<Product> getProductsByPrice(Map<String, List<String>> filterParams) {
        Set<Product> productsByPrice = new HashSet<Product>();

        Set<String> criterias = filterParams.keySet();

        if(criterias.contains("low") && criterias.contains("high")) {
            String low = filterParams.get("low").get(0);
            String high = filterParams.get("high").get(0);

            BigDecimal lowDecimal = new BigDecimal(low);
            BigDecimal highDecimal = new BigDecimal(high);

            productsByPrice.addAll(getProductsByPrice(lowDecimal, highDecimal));
        }

        return productsByPrice;
    }

    public List<Product> getProductByManufacturer(String manufacturer) {
        List<Product> productsByManufacturer = new ArrayList<Product>();
        for(Product product : listOfProducts) {
            if (manufacturer.equalsIgnoreCase(product.getManufacturer())) {
                productsByManufacturer.add(product);
            }
        }
        return productsByManufacturer;
    }

    public void addProduct(Product product) {
        listOfProducts.add(product);
    }
}
