package com.packt.webstore.service;

import com.packt.webstore.domain.Product;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(String productID);
    List<Product> getProductsByCategory(String category);
    Set<Product> getProductsByFilter(Map<String, List<String>> filterParams);
    List<Product> getProductByManufacturer(String manufacturer);
    Set<Product> getProductsByPrice(Map<String, List<String>> filterParams);
    Set<Product> getProductsByCategoryAndManufacturerAndFilter(String category, String manufacturer,
                                                               Map<String, List<String>> filterParams);
    void addProduct(Product product);
}
