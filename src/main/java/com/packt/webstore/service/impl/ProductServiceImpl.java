package com.packt.webstore.service.impl;

import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;
import com.packt.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public Product getProductById(String productID) {
        return productRepository.getProductById(productID);
    }

    public List<Product> getProductsByCategory(String category) {
        return productRepository.getProductsByCategory(category);
    }

    public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
        return productRepository.getProductsByFilter(filterParams);
    }

    public List<Product> getProductByManufacturer(String manufacturer) {
        return productRepository.getProductByManufacturer(manufacturer);
    }

    public Set<Product> getProductsByPrice(Map<String, List<String>> filterParams) {
        return productRepository.getProductsByPrice(filterParams);
    }

    public Set<Product> getProductsByCategoryAndManufacturerAndFilter(String category, String manufacturer,
                                                                      Map<String, List<String>> filterParams) {
        List<Product> productsByCategory = getProductsByCategory(category);
        List<Product> productsByManufacturer = getProductByManufacturer(manufacturer);
        Set<Product> productsByPrice = getProductsByPrice(filterParams);


        Set<Product> productsByCategoryAndManufacturer = new HashSet<Product>();
        Set<Product> productsToReturn = new HashSet<Product>();

        for(Product product : productsByCategory) {
            if (productsByManufacturer.contains(product)) {
                productsByCategoryAndManufacturer.add(product);
            }
        }

        for (Product product : productsByCategoryAndManufacturer) {
            if (productsByPrice.contains(product)) {
                productsToReturn.add(product);
            }
        }

        return productsToReturn;
    }

    public void addProduct(Product product) {
        productRepository.addProduct(product);
    }
}
